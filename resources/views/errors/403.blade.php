@extends('errors.error-layout')
@section('message')
   You don't have permissions to access this page .
   <br>
   Call administrator for access.
@stop
