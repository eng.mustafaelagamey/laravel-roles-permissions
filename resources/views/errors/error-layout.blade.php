@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">System </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @yield('message')
                        <br>
                        <a href="{{url('/')}}"><button class="btn btn-sm btn-secondary">Go home</button></a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
