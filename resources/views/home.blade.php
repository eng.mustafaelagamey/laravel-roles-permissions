@extends('layouts.app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-12  text-center">
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>

                <h1 class="">
                    Welcome To our system
                </h1>
                <br>
                <br>

                <h3>This is your Dashboard</h3>
                <br>

                <h6>Eng.mustafaelagamey@gmail.com</h6>
                <br>
                <p class="btn btn-secondary border rounded">simple users-roles-permissions system</p>
                <br>
                @auth()
                    <div>

                        <p class="btn border rounded">Click on profile to access profile</p>
                        <br>

                        <p class="btn border rounded">Click on users to access users roles and permissions</p>
                    </div>
                @else()
                    <div>

                        <p class="btn border rounded">You aren't logged in
                            <br>
                            ------------------
                            <br>
                            Please Login first
                        </p>


                    </div>
                @endauth


            </div>
        </div>
    </div>
@endsection

