@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        @switch($mode)
                            @case('create')
                            {{ __('Creating') }}
                            @break
                            @case('edit')
                            {{ __('Editing') }}
                            @break

                            @case('show')
                            {{ __('Viewing') }}
                            @break

                        @endswitch
                        {{ __('User') }}

                    </div>
                    <div class="card-body">
                        <form method="POST"
                              @if ($mode!='show')

                              @if ($mode=='create')
                              action="{{ route('users.store') }}"
                              @endif
                              @if ($mode=='edit')
                              action="{{ route('users.update', ['id'=>$user->id])}}"
                            @endif
                            @endif >


                            @if ($mode=='edit')
                                @method('PUT')
                            @endif

                            @csrf

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text"
                                           @if ($mode == 'show' || $mode == 'edit') value="{{$user->name}}" @endif
                                           @if ($mode == 'show' )  readonly @endif

                                           class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                           name="name" value="{{ old('name') }}" required autofocus>

                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">


                                <label for="email"
                                       class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email"
                                           @if ($mode == 'show' || $mode == 'edit') value="{{$user->email}}" readonly
                                           @endif
                                           class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                           name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password" value=""

                                       class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password"
                                           class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                           name="password"
                                           @if ($mode!='edit')
                                           required

                                        @endif
                                    >

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password-confirm"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control"
                                           name="password_confirmation"
                                           @if ($mode!='edit')
                                           required

                                        @endif>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    @if ($mode =='show')
                                        <a href="{{route('users.index')}}"><button type="button"  class="btn btn-outline-dark">Cancel
                                            </button></a>
                                    @else
                                        <button type="submit" class="btn btn-success">
                                            @switch($mode)
                                                @case('create')
                                                {{ __('Create') }}
                                                @break
                                                @case('edit')
                                                {{ __('Apply') }}
                                                @break



                                            @endswitch
                                        </button>
                                    @endif

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @yield('inputs')
@endsection
