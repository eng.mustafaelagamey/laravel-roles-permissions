@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <form method="POST" >
                    @csrf
                    @method('PUT')

                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10">
                            <input type="text" readonly class="form-control-plaintext" id="staticEmail"
                                   value="{{$user->email}}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-2 col-form-label">Name</label>
                        <div class="col-sm-10">
                            <input type="text" readonly class="form-control-plaintext" id="staticEmail"
                                   value="{{$user->name}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-2 col-form-label">Roles</label>
                        <div class="col-sm-10">
                            @foreach($roles as $role)
                                <div class="form-check form-check-inline">
                                    <?php $selectedRoles = $user->roles->pluck('id') ?>
                                    <input name="roles[]"
                                        @if(in_array($role->id,$selectedRoles->toArray()))checked="checked" @endif
                                    class="form-check-input" type="checkbox" id="role{{$role->slug}}"
                                        value="{{$role->slug}}">
                                    <label class="form-check-label" for="role{{$role->slug}}">{{$role->name}}</label>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="staticEmail" class="col-sm-2 col-form-label">Additional Permissions</label>
                        <div class="col-sm-10">
                            @foreach($permissions as $permission)
                                <div class="form-check form-check-inline">
                                    <?php $selectedRoles = $user->permissions->pluck('id') ?>
                                    <input name="permissions[]"
                                        @if(in_array($permission->id,$selectedRoles->toArray()))checked="checked" @endif
                                    class="form-check-input" type="checkbox" id="permission{{$permission->slug}}"
                                        value="{{$permission->slug}}">
                                    <label class="form-check-label" for="permission{{$permission->slug}}">{{$permission->name}}</label>
                                </div>
                            @endforeach
                        </div>
                    </div>

                    <button type="submit" class="btn btn-success">Save</button>
                    <button type="button" class="btn btn-dark">Cancel</button>

                </form>
            </div>
        </div>
    </div>

@endsection

