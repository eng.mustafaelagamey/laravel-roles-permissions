@extends('layouts.app')
@section('content')

    <div class="container-fluid">
        @can('create-users')
            <a href="{{route('users.create')}}">
                <button class="btn btn-success">Create User</button>
            </a>
        @endcan()
        <table class="table table-striped">

            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Email</th>
                <th scope="col">Name</th>
                <th scope="col">Roles</th>
                <th scope="col">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <th scope="row">{{$user->id}}</th>
                    <td>{{$user->email}}</td>
                    <td>{{$user->name}}</td>
                    <td>{{implode(' , ',$user->roles->pluck('name')->toArray())}}</td>
                    <td>
                        @can('update-permissions')

                            <a class="" href="{{route('user-edit-permissions',['id'=>$user->id])}}">
                                <button class="btn btn-primary">Permissions</button>

                            </a>
                        @endcan()

                        @can('update-users')
                            <a class="" href="{{route('users.edit',['id'=>$user->id])}}">
                                <button class="btn btn-secondary">Edit</button>
                            </a>
                        @endcan()
                        @can('view-users')
                            <a class="" href="{{route('users.show',['id'=>$user->id])}}">
                                <button class="btn btn-outline-secondary">View</button>
                            </a>
                        @endcan()


                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection

