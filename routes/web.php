<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('home');
});
Route::get('/home', 'HomeController@home')->name('home');

Auth::routes();


Route::resource('users', 'UserController');
//Route::get('/users', 'UserController@users')->name('users')->middleware('permission:view-users');
//Route::get('/users/create', 'UserController@create')->name('users.create')->middleware('permission:view-users');
//Route::post('/users', 'UserController@store')->name('users.store')->middleware('permission:view-users');
//Route::get('/users/{id}/edit', 'UserController@create')->name('users.create')->middleware('permission:view-users');
//Route::post('/users', 'UserController@store')->name('users.store')->middleware('permission:view-users');
//


Route::get('/user-edit-permissions/{id}', 'UserController@userEditPermissions')->name('user-edit-permissions')->middleware('permission:update-permissions');
Route::put('/user-edit-permissions/{id}', 'UserController@userUpdatePermissions')->middleware('permission:view-users');


Route::get('/profile', 'UserController@show')->name('profile');
