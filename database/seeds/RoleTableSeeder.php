<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $permissions = \App\Permission::all();
        foreach (\App\Role::names as $slug => $name) {
            $role = factory(App\Role::class)->create(['slug' => $slug, 'name' => $name]);
            if ($slug == 'admin') {
                $role->permissions()->attach($permissions);
            }
        }
    }
}
