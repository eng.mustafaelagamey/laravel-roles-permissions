<?php

use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        foreach (\App\Permission::generatePermissions() as $slug => $name) {
             factory(App\Permission::class)->create(['slug' => $slug, 'name' => $name]);
        }
        factory(App\Permission::class)->create(['slug' => 'update-permissions', 'name' => 'Update permissions']);

    }
}
