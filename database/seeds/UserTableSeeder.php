<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        foreach (\App\Role::all() as $role) {
            foreach ([1, 2, 3] as $count) {
                $user = factory(App\User::class)
                    ->create(['name' => $role->name . ' #' . $count,
                        'email' => $role->slug . $count . '@test.test',]);
                $user->roles()->attach($role);
            }
        }


    }
}
