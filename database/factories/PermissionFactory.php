<?php

use Faker\Generator as Faker;

$factory->define(App\Permission::class, function (Faker $faker) {
    $slug = array_rand(\App\Permission::names);
    return [
        'name' => \App\Permission::names[$slug],
        'slug' => $slug,
    ];
});
