<?php

namespace App\Providers;

use App\Permission;
use App\User;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class PermissionsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // this try for checking if can get permissions from table
        try {

            //defining gates for each permission to use laravel core
            Permission::get()->map(function ($permission) {
                Gate::define($permission->slug, function (User $user) use ($permission) {
                    return $user->hasPermissionTo($permission);
                });
            });
        } catch (\Exception $exception) {

        }

        // creating blade for using it to check by role

        Blade::directive('role', function ($role) {
            return "<?php if( auth()->check() && auth()->user()->hasRole({$role}) ) : ?>";
        });
        Blade::directive('endrole', function () {
            return "<?php endif ; ?>";
        });


    }
}
