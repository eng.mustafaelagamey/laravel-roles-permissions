<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Auth\RegisterController;
use App\Permission;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {


        $this->middleware('auth')->except('home');
    }



    /**
     * listing all users
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        // checking permission for accessing index
        $this->authorize('index', User::class);

        // eager loading roles for users
        $users = User::with('roles')->get();

        // displaying list view
        return view('users.index', compact('users'));
    }


    /**
     *
     * get all permissions for users page
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function userEditPermissions($id)
    {
        // get requested user
        $user = User::with('roles')->find($id);
        // get all roles to list them later
        $roles = Role::all();
        // get all permissions to list them later
        $permissions = Permission::all();
        // displaying permissions page
        return view('users.permissions', compact('user', 'roles', 'permissions'));
    }


    /**
     * updating user permissions
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function userUpdatePermissions($id)
    {
        // find edited user
        $user = User::find($id);

        // get the old user roles by slugs
        $givenRoles = $user->roles->pluck('slug');
        // collecting the new roles
        $newRoles = collect(\request('roles'));
        // get the new roles
        $recentlyAddedRoles = $newRoles->diff($givenRoles);
        // get the removed roles
        $recentlyRemovedRoles = $givenRoles->diff($newRoles);
        // removing unchecked roles
        $user->removeRolesFrom($recentlyRemovedRoles->toArray());
        // saving the recently new roles for the user
        $user->giveRoleTo($recentlyAddedRoles->toArray());

        // doing same as roles
        $givenPermissions = $user->permissions->pluck('slug');
        $newPermissions = collect(\request('permissions'));
        $recentlyAddedPermissions = $newPermissions->diff($givenPermissions);
        $recentlyRemovedPermissions = $givenPermissions->diff($newPermissions);
        $user->removePermissionsFrom($recentlyRemovedPermissions->toArray());
        $user->givePermissionTo($recentlyAddedPermissions->toArray());

        // redirecting to index
        return redirect()->route('users.index')->with('message', ['class' => 'alert-success', 'value' => 'Success']);

    }

    public function create()
    {
        // checking gate for creating user
        $this->authorize('create', User::class);

        return view('users.create');
    }

    public function edit($id)
    {
        $user = User::find($id);
        return view('users.edit', compact('user'));
    }

    public function store()
    {
        // validating the inserted data
        \request()->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        $user = User::create([
            'name' => \request('name'),
            'email' => \request('email'),
            'password' => Hash::make(\request('password')),
        ]);

        return redirect()->route('user-edit-permissions', ['id' => $user->id])->with('message', ['class' => 'alert-success', 'value' => 'Success'])->with(compact('user'));
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update($id)
    {

        $user = User::find($id);
        $request = \request()->all();
        // validating only name
        $this->validate(\request(), [
            'name' => ['required', 'string', 'max:255'],
        ]);
        // removing password
        if ($request['password'] == '') {
            unset($request['password']);
        } else {
            $this->validate(\request(), [
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ]);
            $user->password = Hash::make(\request('password'));
        }
        // saving only name to the old user
        $user->name = \request('name');

        $user->save();

        return redirect()->route('users.index', ['id' => $user->id])->with('message', ['class' => 'alert-success', 'value' => 'Success'])->with(compact('user'));
    }

    /**
     * display any user profile or the authenticated user profile
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show($id = null)
    {

        if ($id === null) {

            $user = Auth::user();
        } else {
            $user = User::find($id);
        }
        $this->authorize('profile', $user);

        return view('users.show', compact('user'));
    }


}
