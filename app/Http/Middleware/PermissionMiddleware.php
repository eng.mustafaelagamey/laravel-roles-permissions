<?php

namespace App\Http\Middleware;

use Closure;

class PermissionMiddleware
{
    /**
     *
     * Handling route permissions before entring controller
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permission)
    {
        if (
            !$request->user()->permissions()->whereSlug($permission)->count()
            and !$request->user()->indirectPermissions($permission)
        ) {

            abort(403);
        }
        return $next($request);
    }
}
