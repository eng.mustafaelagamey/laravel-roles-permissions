<?php
/**
 * Created by PhpStorm.
 * User: mustafa
 * Date: 4/4/19
 * Time: 1:55 AM
 */

namespace App;


trait PermissionsTrait
{

    // creating Permission Trait to use it inside users Model

    /**
     * check for specific roles assigned to user
     * @param mixed ...$roles
     * @return bool
     */
    public function hasRole(...$roles)
    {
        foreach ($roles as $role) {
            if ($this->roles->contains('slug', $role)) {
                return true;
            }
        }
        return false;
    }


    /**
     * assign role to user
     *
     * @param array $roles
     * @return $this
     */
    public function giveRoleTo(array $roles)
    {

        $roles = Role::getAllroles($roles);
        if ($roles === null) {
            /// must be check
            return $this;
        }
        $this->roles()->saveMany($roles);

    }

    /**
     * deleting roles from user
     * @param array $roles
     * @return $this
     */
    public function removeRolesFrom(array $roles)
    {
        $roles = Role::getAllroles($roles);
        if ($roles === null) {
            /// must be check
            return $this;
        }

        $this->roles()->detach($roles);
    }


    /**
     * get the permissions which user can get it from his roles
     *
     * @param $permission
     * @return bool
     */
    public function indirectPermissions($permission)
    {

        if (gettype($permission)=='object' && get_class($permission) == Permission::class) {

        } else {
            $permission = Permission::with('roles')->whereSlug($permission)->first();
        }


        if ($permission)
            foreach ($permission->roles as $role) {
                if ($this->roles->contains($role)) {
                    return true;
                }
            }
        return false;
    }

//    }

    /**
     * check for permissions
     *
     * @param $permission
     * @return bool
     */
    public function hasPermissionTo($permission)
    {
        return $this->hasPermission($permission);

    }

    public function hasPermission($permission)
    {
        if (gettype($permission)=='object' && get_class($permission) == Permission::class) {
            $permissionSlug = $permission->slug;
        } else {
            $permissionSlug = $permission;
        }


        return ($this->permissions->where('slug', $permissionSlug)->count() > 0
            || $this->indirectPermissions($permission));
    }


    /**
     * assign rules to user
     *
     * @param array $permissions
     * @return $this
     */
    public function givePermissionTo(array $permissions)
    {

        $permissions = Permission::getAllpermissions($permissions);

        if ($permissions === null) {
            /// must be check
            return $this;
        }

        $this->permissions()->saveMany($permissions);

    }

    /**
     * removing permissions from user
     * @param array $permissions
     * @return $this
     */
    public function removePermissionsFrom(array $permissions)
    {
        $permissions = Permission::getAllpermissions($permissions);
        if ($permissions === null) {
            /// must be check
            return $this;
        }
        $this->permissions()->detach($permissions);
    }


    /*
     * building relationships
     */

    public function permissions()
    {
        return $this->belongsToMany(Permission::class, 'users_permissions');
    }
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'users_roles');
    }

}
