<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{

    public const names = [
        'users' => 'Users',
    ];
    public const actions = [
        'create' => 'Create',
        'view' => 'View',
        'update' => 'Update',
        'delete' => 'Delete',
    ];

    /**
     * auto create all data
     * @return array
     */
    public static function generatePermissions(){
        $permissions = [];
        foreach (self::actions as $action => $actionName) {
            foreach (self::names as $model => $modelName) {
                $permissions[$action . '-' . $model] = $actionName . ' ' . $modelName;
            }
        }
        return $permissions;
    }

    // getting specific permissions
    public static function getAllPermissions(array $permissions)
    {
        return Permission::whereIn('slug', $permissions)->get();
    }

    //relations for permission

    public function users()
    {
        return $this->belongsToMany(User::class, 'users_permissions');
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'roles_permissions');
    }




}
